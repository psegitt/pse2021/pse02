# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 13:35:20 2021

@author: bubib
"""
import labcomdig as lab
import numpy as np
import matplotlib.pyplot as plt


L = 64
wc = 8*np.pi/L
#Np = np.arange(0,np.pi,np.pi/10**4) Continuo
Np = np.arange(0,L,1)
gn = np.sin(Np*np.pi/L)

Bn = np.random.randint(0,2,10**4)
Eb = 8
M = 8
P = gn*np.cos(wc*Np)


[Xn,Bn2,An,phi,alfabeto] = lab.transmisorpam(Bn,Eb,M,P,L)

tm = 2/L/10**4

#plt.stem(P)
plt.plot(Xn[0:L*4])
plt.plot(np.arange(L*4)*np.sqrt(tm),Xn[0:L*4])


