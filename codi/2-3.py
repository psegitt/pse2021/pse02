
import numpy as np
import matplotlib.pyplot as plt

Np = 10**6 # Número de muestras a generar
Nbins = 20 # Número de intervalos en los que se dividirá el histograma
Xu = np.random.rand(Np) # Muestras de v.a. uniformemente distribuida entre 0 y 1
plt.figure(5)
# Se representa un subconjunto de las muestras, las 100 primeras, en el eje x.
plt.plot(Xu[:100], np.zeros(100),'.r') #se utiliza marcador '.', en rojo
# Se fijan los límites de representación
plt.axis([-0.25, 1.25, -0.125, 1.125])
plt.plot(np.array([-0.5, 1.5]),np.array([0, 0]),'b')
# Se estima la fpd, usando la función histograma
[fx,xedge] = np.histogram(Xu,Nbins); #Calcula el histograma: Nbins es el número de contenedores en los que se divide el intervalo entre el valor mínimo y máximo de Xu.
# La función devuelve en xedge las coordenadas iniciales y finales de cada uno de los contenedores: en total Nbins+1 valores. En la i-ésima entrada de fx devuelve el número de veces que la va toma un valor perteneciente al i-ésimo intervalo.
# El número de muestras en cada intervalo divido por el número total de muestras generadas de la va es la frecuencia relativa de cada intervalo. Para obtener la estimación de la fdp hay que dividir por el ancho del intervalo.
fx = fx/Np/(xedge[1]-xedge[0])
# Se representa la estimación de fx mediante un diagrama de barras.
plt.bar(xedge[:-1],fx,width=((xedge[1]-xedge[0])*0.8),align='edge',color='grey')
# Las coordenadas de los centros de cada intervalo: en total Nbins coordenadas
##x=(xedge[:-1]+xedge[1:])/2
# Se representa la función con dos puntos adicionales para señalar la subida y bajada de la misma
##plt.plot(x,fx,'r',linewidth=2)
##plt.plot([0,x[0]],[0,fx[0]],'r',linewidth=2)
##plt.show()