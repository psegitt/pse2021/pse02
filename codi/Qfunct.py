# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 12:53:20 2021

@author: bubib
"""




def Qfunct():
    import numpy as np
    import matplotlib.pyplot as plt
    
    xdB = np.arange(0,20,0.1)
    x = 10**(xdB/10)
    
    plt.semilogy(xdB,Qfunct(np.sqrt(x)))
    plt.show()
    
    return []