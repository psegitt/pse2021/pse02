# -*- coding: utf-8 -*-
"""
Created on Wed Nov 10 13:23:36 2021

@author: bubib
"""

import numpy as np
import matplotlib.pyplot as plt
from labcomdig import LCDfdp

Np1 = 10**6
Np2 = 10**6
sigma = 4
Xu1 = (np.random.randn(Np1,1)*np.sqrt(sigma))+1
Xu2 = (np.random.randn(Np2,1)*np.sqrt(sigma))-1
Xuz = Xu1 + Xu2
Nbins = 20

plt.figure(5)
#plt.plot(Xu1[:100], np.zeros(100),'.r')
#plt.plot(np.array([-0.5, 1.5]),np.array([0, 0]),'b')

deltax = 0.1
ejex = np.arange(-10,10+deltax,deltax)
[fx1,x1] = LCDfdp(Xu1,ejex)
[fx2,x2] = LCDfdp(Xu2,ejex)
[fxz,xz] = LCDfdp(Xuz,ejex)

plt.plot(x1,fx1)
plt.plot(x2,fx2)
plt.plot(xz,fxz)