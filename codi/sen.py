# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 14:04:01 2021

@author: bubib
"""

def sen(L,T,Eg):
    import numpy as np
    import matplotlib.pyplot as plt
    
    A=np.sqrt(Eg)
    
    tm = T/L
    n= np.arange(0,L,1)
    gn = np.sqrt(Eg)*np.sin(np.pi*n/L)//A
    
    gt = np.sqrt(1/tm)*gn
    plt.stem(gt)
    
    return