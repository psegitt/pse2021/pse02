# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 13:25:42 2021

@author: bubib
"""

def grafica(L,T):
    import numpy as np
    import matplotlib.pyplot as plt
    
    tm=T/L
    gn = np.sqrt(tm)*np.ones(L)
    gn[L//2:1] = -1
    
    plt.stem(gn)
    
    Eng = gn@gn
    
    return Eng