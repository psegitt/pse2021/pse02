


import numpy as np
import matplotlib.pyplot as plt
from labcomdig import LCDfdp , LCDfmp

Np1 = 10**6
Np2 = 10**6
sigma = 4
Xu1 = (np.random.randn(Np1,1)*np.sqrt(sigma))+1
Xu2 = np.sign(np.random.randint(4,size=Np2)-2.5)
Xuz = Xu1 + Xu2
Nbins = 20

plt.figure(5)

deltax = 0.1
ejex = np.arange(-10,10+deltax,deltax)
[fx1,x1] = LCDfdp(Xu1,ejex)
[fx2,x2] = LCDfmp(Xu2)
[fxz,xz] = LCDfdp(Xuz,ejex)

plt.plot(x1,fx1)
plt.plot(x2,fx2)
plt.plot(xz,fxz)