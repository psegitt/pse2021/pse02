# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 14:21:44 2021

@author: bubib
"""

def sen(N,A):
    import numpy as np
    import matplotlib.pyplot as plt
    
    return 2*A*(np.random.rand(N,1)-0.5)