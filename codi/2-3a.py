# -*- coding: utf-8 -*-
"""
Created on Wed Nov 10 13:13:18 2021

@author: bubib
"""
import numpy as np
import matplotlib.pyplot as plt

Np = 10**6
Xu = np.random.rand(Np)
Nbins = 20

plt.figure(5)
plt.plot(Xu[:100], np.zeros(100),'.r')
plt.plot(np.array([-0.5, 1.5]),np.array([0, 0]),'b')

deltax = 0.1
ejex = np.arange(-2,6+deltax,deltax)
[fx,xedge] = np.histogram(Xu,ejex)

plt.bar((xedge[0 : -1]+xedge[0 :])/2,fx,width=((xedge[1]-xedge[0])*0.8),align='edge',color='grey')