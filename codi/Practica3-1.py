import numpy as np
import matplotlib.pyplot as plt
from labcomdig import LCDfdp , LCDfmp


Npx = 10**6
Np1 = 10**6
Np2 = 10**6


Xu = -1*np.sign(np.random.randint(4,size=Npx)-2.5)
N1 = (np.random.rand(Np1,1)*2)-1
N2 = (np.random.rand(Np2,1)*2)-1
N = N1 + N2

NX1 = N+1
NX2 = N-1

deltax = 0.1
ejex = np.arange(-4,4+deltax,deltax)

[fx,x] = LCDfmp(Xu)
#[fn1,x1] = LCDfdp(N1,ejex)
#[fn2,x2] = LCDfdp(N2,ejex)
[fn,xn] = LCDfdp(N,ejex)
[fnX1,xnX1] = LCDfdp(NX1,ejex)
[fnX2,xnX2] = LCDfdp(NX2,ejex)

XalfaMap = ejex[np.where(fnX2*3/4<fnX1/4)[0][0]]
XalfaML = ejex[np.where(fnX2<fnX1)[0][0]]

PeX1Map = np.sum(fnX1[:np.where(fnX2*3/4<fnX1/4)[0][0]])*deltax
PeX2Map = np.sum(fnX2[np.where(fnX2*3/4<fnX1/4)[0][0]:])*deltax
PetMap = (PeX1Map*1/4)+(PeX2Map*3/4)

PeX1ML = np.sum(fnX1[:np.where(fnX2<fnX1)[0][0]])*deltax
PeX2ML = np.sum(fnX2[np.where(fnX2<fnX1)[0][0]:])*deltax
PetML = (PeX1ML*1/4)+(PeX2ML*3/4)

#plt.axis([-4,4,0,1])

#Apartado 1
#plt.plot(x,fx)

#Apartado 2
#plt.plot(x1,fn1)
#plt.plot(x2,fn2)
#plt.plot(xn,fn)

#Apartado 3
#plt.plot(xnX1,fnX1)
#plt.plot(xnX2,fnX2)

#Apartado 4
#plt.plot(xnX1,fnX1/4)
#plt.plot(xnX2,fnX2*3/4)

#Apartado 5
#print(XalfaMap)
#print(XalfaML)

#Apartado 6 y 7
print(PetMap)
print(PetML)
