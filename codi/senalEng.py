# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 13:55:41 2021

@author: bubib
"""

def graficaEng(L,T,eng):
    import numpy as np
    import matplotlib.pyplot as plt
    
    tm=T/L
    gn=np.ones(L)
    gn = np.sqrt(eng)*np.ones(L)
    gn[L//2:1] = -1
    
    plt.stem(gn)

    return A